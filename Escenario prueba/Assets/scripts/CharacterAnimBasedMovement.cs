using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAnimBasedMovement : MonoBehaviour
{

    public float RotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float DegreesToRun = 160f;

    [Header("Animator Parameters")]

    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";

    [Header("Animator Parameters")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private Ray wallRay = new Ray();
    private float Speed;
    private Vector3 DesiredDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }


    private void OnAnimatorIK(int layerIndex)
    {
        if (Speed < rotationThreshold) return;

        float DistanceToleftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float DistanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if (DistanceToRightFoot > DistanceToleftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }

    public void MoveCharacter(float HInput, float VInput, Camera Cam, bool jump, bool dash)
    {
        Speed = new Vector2(HInput, VInput).normalized.sqrMagnitude;

        if(Speed >= Speed - rotationThreshold && dash)
        {
            Speed = 1.5f;
        }

        if (Speed >= rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = Cam.transform.forward;
            Vector3 right = Cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            DesiredDirection = forward * VInput + right * HInput;


            if (Vector3.Angle(transform.forward, DesiredDirection) >= DegreesToRun)
            {
                turn180 = true;

            }
            else
            {

                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(DesiredDirection), RotationSpeed * Time.deltaTime);

            }
            animator.SetBool(turn180Param, turn180);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);


        }
        else if (Speed < rotationThreshold)
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }




    }
}
