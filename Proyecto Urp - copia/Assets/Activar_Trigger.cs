using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Activar_Trigger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private UnityEvent evento;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            evento.Invoke();
        }
    }
}
