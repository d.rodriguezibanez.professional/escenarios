using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    private InputData input;
    private CharacterAnimBasedMovement CharacterMovement;

    private void Start()
    {
        CharacterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    private void Update()
    {
        input.GetInput();

        CharacterMovement.MoveCharacter(input.Hmovement, input.Vmovement, cam, input.Jump, input.Dash);
    }

}
