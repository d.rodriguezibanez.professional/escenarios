using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public struct InputData
{

    public float Hmovement;
    public float Vmovement;

    public float VerticalMouse;
    public float HorizontalMouse;

    public bool Dash;
    public bool Jump;

    public void GetInput()
    {
        Hmovement = Input.GetAxis("Horizontal");
        Vmovement = Input.GetAxis("Vertical");

        VerticalMouse = Input.GetAxis("Mouse Y");
        HorizontalMouse = Input.GetAxis("Mouse X");

        Dash = Input.GetButton("Dash");
        Jump = Input.GetButtonDown("Jump");
    }
}
